package com.matevskial.bookstore.dto;

import com.matevskial.bookstore.persistence.entity.Book;
import com.matevskial.bookstore.persistence.entity.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookInputDtoTest {

    private BookInputDto bookInputDto;

    @BeforeEach
    public void setUp() {
        bookInputDto = new BookInputDto();
        bookInputDto.setId(1L);
        bookInputDto.setTitle("Individualism");
        bookInputDto.setGenre("nonfiction");

    }

    @Test
    public void toEntityShouldWork() {

        // Given
        Book expectedBook = new Book();
        expectedBook.setId(bookInputDto.getId());
        expectedBook.setTitle(bookInputDto.getTitle());
        expectedBook.setGenre(Genre.valueOf(bookInputDto.getGenre()));

        // When
        Book book = bookInputDto.toEntity();

        // Then
        assertEquals(expectedBook, book);
    }
}