package com.matevskial.bookstore.service.impl;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.exception.BookStoreException;
import com.matevskial.bookstore.persistence.entity.Author;
import com.matevskial.bookstore.persistence.repository.AuthorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class AuthorServiceImplTest {

    @Autowired
    AuthorServiceImpl authorService;

    @MockBean
    AuthorRepository authorRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAuthorByIdShouldWork() {

        // Given
        long authorId = 1L;
        Author author = new Author();
        author.setId(authorId);
        Optional<Author> authorOptional = Optional.of(author);

        // Set up behavior of mocked objects
        when(authorRepository.findById(authorId)).thenReturn(authorOptional);

        // When
        AuthorDto resultAuthor = authorService.getAuthorById(authorId);

        // Then
        verify(authorRepository, times(1)).findById(authorId);
    }

    @Test
    void getAuthorByIdShouldThrowException() {

        // Given
        long authorId = 1L;
        Optional<Author> authorOptional = Optional.empty();

        // Set up behavior of mocked objects
        when(authorRepository.findById(authorId)).thenReturn(authorOptional);

        // When, Then
        assertThrows(BookStoreException.class, () -> authorService.getAuthorById(authorId));
    }

    @Test
    void saveAuthor() {
    }
}