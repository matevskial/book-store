package com.matevskial.bookstore.exception;

public class BookStoreException extends RuntimeException {

    public BookStoreException(String message) {
        super(message);
    }
}
