package com.matevskial.bookstore.service;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.exception.BookStoreException;
import com.matevskial.bookstore.persistence.entity.Author;

/**
 * A service to provide business logic for authors
 */
public interface AuthorService {

    /**
     * Method to get one author by its id from a data source.
     *
     * @param authorId - id of the author
     *
     * @return author with specified id
     *
     * @throws BookStoreException
     *  * when author with specified id doesn't exist in the data source
     */
    AuthorDto getAuthorById(long authorId) throws BookStoreException;

    /**
     * Method to persist new author in a data source
     *
     * @param author - author to be persisted
     *
     * @return saved Author
     */
    AuthorDto saveAuthor(AuthorDto author);
}
