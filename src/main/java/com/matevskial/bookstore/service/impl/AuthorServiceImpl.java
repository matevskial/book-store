package com.matevskial.bookstore.service.impl;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.exception.BookStoreException;
import com.matevskial.bookstore.persistence.entity.Author;
import com.matevskial.bookstore.persistence.repository.AuthorRepository;
import com.matevskial.bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Override
    @Transactional
    public AuthorDto getAuthorById(long authorId) throws BookStoreException {
        Author author = authorRepository.findById(authorId).orElseThrow(() -> {
            throw new BookStoreException(
                    String.format("Author with id %s not found", authorId)
            );
        });

        return AuthorDto.fromEntity(author);
    }

    @Override
    @Transactional
    public AuthorDto saveAuthor(AuthorDto authorDto) {
        Author author = authorDto.toEntity();
        return AuthorDto.fromEntity(authorRepository.save(author));
    }
}
