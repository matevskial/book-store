package com.matevskial.bookstore.service.impl;

import com.matevskial.bookstore.dto.BookInputDto;
import com.matevskial.bookstore.dto.BookOutputDto;
import com.matevskial.bookstore.exception.BookStoreException;
import com.matevskial.bookstore.persistence.entity.Book;
import com.matevskial.bookstore.persistence.entity.Genre;
import com.matevskial.bookstore.persistence.repository.AuthorRepository;
import com.matevskial.bookstore.persistence.repository.BookRepository;
import com.matevskial.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * A service to provide data access and manipulation of books
 */
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    @Override
    @Transactional
    public List<BookOutputDto> getAllBooks() {
        return BookOutputDto.fromEntityList(bookRepository.findAll());
    }

    @Override
    @Transactional
    public BookOutputDto getBookById(long bookId) throws BookStoreException {
        Book book = bookRepository.findById(bookId).orElseThrow(() -> {
            throw new BookStoreException(
                    String.format("Book with id %s not found", bookId)
            );
        });

        return BookOutputDto.fromEntity(book);
    }

    @Override
    @Transactional
    public BookOutputDto saveBook(BookInputDto bookInputDto) {
        return saveOrUpdateBook(bookInputDto);
    }

    @Override
    @Transactional
    public BookOutputDto updateBook(BookInputDto bookInputDto) throws BookStoreException {

        if(bookInputDto.getId() == null || !bookRepository.existsById(bookInputDto.getId())) {
            throw new BookStoreException(
                    String.format("Book with id %s not found", bookInputDto.getId())
            );
        }

        return saveOrUpdateBook(bookInputDto);
    }

    private BookOutputDto saveOrUpdateBook(BookInputDto bookInputDto) {
        for(Long authorId : bookInputDto.getAuthors()) {
            if(!authorRepository.existsById(authorId)) {
                throw new BookStoreException(
                        String.format("Author with id %s not found", authorId)
                );
            }
        }

        if(bookInputDto.getGenre() == null || !Genre.exists(bookInputDto.getGenre())) {
            throw new BookStoreException(
                    String.format("Genre '%s' is not a valid book genre", bookInputDto.getGenre())
            );
        }

        Book book = bookInputDto.toEntity();
        return BookOutputDto.fromEntity(bookRepository.save(book));
    }

    @Override
    @Transactional
    public void deleteBook(long bookId) throws BookStoreException {
        if(!bookRepository.existsById(bookId)) {
            throw new BookStoreException(
                    String.format("Book with id %s not found", bookId)
            );
        }
        bookRepository.deleteById(bookId);
    }
}
