package com.matevskial.bookstore.service;

import com.matevskial.bookstore.dto.BookInputDto;
import com.matevskial.bookstore.dto.BookOutputDto;
import com.matevskial.bookstore.exception.BookStoreException;
import com.matevskial.bookstore.persistence.entity.Book;

import java.util.List;

/**
 * A service to provide business logic for books
 */
public interface BookService {

    /**
     * Method to get all books from a data source
     *
     * @return java.util.List of books
     */
    List<BookOutputDto> getAllBooks();

    /**
     * Method to get one book by its id from a data source.
     *
     * @param bookId id of the book
     *
     * @return Book with specified id
     *
     * @throws BookStoreException
     *  * when book with specified id doesn't exist in the data source
     */
    BookOutputDto getBookById(long bookId) throws BookStoreException;

    /**
     * Method to persist new book in a data source
     *
     * @param bookInputDto - book to be persisted
     *
     * @return saved Book
     *
     * @throws BookStoreException
     *  * when at least one of the associated authors of the book doesn't exist
     */
    BookOutputDto saveBook(BookInputDto bookInputDto);

    /**
     * Method to update a book if it already exists in the data source
     *
     * @param bookInputDto - book to be updated
     *
     * @return updated Book
     *
     * @throws BookStoreException
     *  * when id of the book object is null or doesn't exist in the data source
     *  * when at least one of the associated authors of the book doesn't exist
     */
    BookOutputDto updateBook(BookInputDto bookInputDto) throws BookStoreException;

    /**
     * Method to delete a book specified with its id from a data source
     *
     * @param bookId - id of the book to be deleted
     *
     * @throws BookStoreException
     *  * when book with specified id doesn't exist in the data source
     */
    void deleteBook(long bookId) throws BookStoreException;
}
