package com.matevskial.bookstore.graphql.resolvers;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.dto.BookOutputDto;
import com.matevskial.bookstore.service.AuthorService;
import com.matevskial.bookstore.service.BookService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class Query implements GraphQLQueryResolver {

    private final BookService bookService;
    private final AuthorService authorService;

    public List<BookOutputDto> getBooks() {
        return bookService.getAllBooks();
    }

    public BookOutputDto getBookById(Long bookId) {
        return bookService.getBookById(bookId);
    }

    public AuthorDto getAuthorById(Long authorId) {
        return authorService.getAuthorById(authorId);
    }
}
