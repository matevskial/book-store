package com.matevskial.bookstore.graphql.resolvers;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.dto.BookInputDto;
import com.matevskial.bookstore.dto.BookOutputDto;
import com.matevskial.bookstore.service.AuthorService;
import com.matevskial.bookstore.service.BookService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Mutation implements GraphQLMutationResolver {

    private final BookService bookService;
    private final AuthorService authorService;

    public BookOutputDto addBook(BookInputDto bookInputDto) {
        return bookService.saveBook(bookInputDto);
    }

    public BookOutputDto updateBook(BookInputDto bookInputDto) {
        return bookService.updateBook(bookInputDto);
    }

    public String deleteBook(Long bookId) {
        bookService.deleteBook(bookId);
        return String.format("Book with id %s deleted", bookId);
    }

    public AuthorDto addAuthor(AuthorDto authorDto) {
        return authorService.saveAuthor(authorDto);
    }
}
