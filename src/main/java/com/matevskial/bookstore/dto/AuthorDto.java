package com.matevskial.bookstore.dto;

import com.matevskial.bookstore.persistence.entity.Author;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * DTO class for both input and output
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorDto {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static AuthorDto fromEntity(Author author) {
        return new AuthorDto(author.getId(), author.getFirstName(), author.getLastName(), author.getDateOfBirth());
    }

    public Author toEntity() {
        return new Author(id, firstName, lastName, dateOfBirth);
    }
}
