package com.matevskial.bookstore.dto;

import com.matevskial.bookstore.persistence.entity.Author;
import com.matevskial.bookstore.persistence.entity.Book;
import com.matevskial.bookstore.persistence.entity.Genre;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto class for the book entity
 * Useful for transferring the book to be added or modified
 * from the controller to the service
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookInputDto {

    private Long id;
    private String title;
    private List<Long> authors = new ArrayList<>();
    private String genre;

    public Book toEntity() {
        Book book = new Book();
        book.setId(id);
        book.setTitle(title);
        book.setGenre(Genre.valueOf(genre.toUpperCase()));

        for(Long authorId : authors) {
            Author author = new Author();
            author.setId(authorId);
            book.getAuthors().add(author);
        }

        return book;
    }
}
