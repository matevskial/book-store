package com.matevskial.bookstore.dto;

import com.matevskial.bookstore.persistence.entity.Author;
import com.matevskial.bookstore.persistence.entity.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookOutputDto {

    private Long id;
    private String title;
    private List<AuthorDto> authors;
    private String genre;

    public static BookOutputDto fromEntity(Book book) {
        List<AuthorDto> authorDtos = new ArrayList<>();
        for(Author a : book.getAuthors()) {
            authorDtos.add(AuthorDto.fromEntity(a));
        }
        return new BookOutputDto(book.getId(), book.getTitle(), authorDtos, book.getGenre().name().toLowerCase());
    }

    public static List<BookOutputDto> fromEntityList(List<Book> books) {
        List<BookOutputDto> result = new ArrayList<>();

        for(Book b : books) {
            result.add(fromEntity(b));
        }

        return result;
    }
}
