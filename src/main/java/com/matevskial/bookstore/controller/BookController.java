package com.matevskial.bookstore.controller;

import com.matevskial.bookstore.dto.BookInputDto;
import com.matevskial.bookstore.dto.BookOutputDto;
import com.matevskial.bookstore.persistence.entity.Book;
import com.matevskial.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller that exposes the books resource
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    /**
     * Method to get all books
     *
     * @return java.util.List of books wrapped in ResponseEntity
     */
    @GetMapping
    public ResponseEntity<List<BookOutputDto>> getAllBooks() {
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    /**
     * Method to get one book by its id
     *
     * @param bookId - id of the book passed as path variable
     *
     * @return Book with specified id wrapped in ResponseEntity if the book exists
     */
    @GetMapping("{bookId}")
    public ResponseEntity<BookOutputDto> getBookById(@PathVariable Long bookId) {
        return ResponseEntity.ok(bookService.getBookById(bookId));
    }

    /**
     * Method to persist new book
     *
     * @param bookInputDto - book to be persisted
     *
     * @return Empty ResponseEntity with status code OK if persisting a new book is successful
     */
    @PostMapping
    public ResponseEntity<Void> saveBook(@RequestBody BookInputDto bookInputDto) {
        bookService.saveBook(bookInputDto);
        return ResponseEntity.ok().build();
    }

    /**
     * Method to update a book if it already exists
     *
     * @param bookInputDto - book to be updated
     *
     * @return Empty ResponseEntity with status code OK if updating is successful
     */
    @PutMapping
    public ResponseEntity<Void> updateBook(@RequestBody BookInputDto bookInputDto) {
        bookService.updateBook(bookInputDto);
        return ResponseEntity.ok().build();
    }

    /**
     * Method to delete a book specified with its id
     *
     * @param bookId - id of the book to be deleted
     *
     * @return Empty ResponseEntity with status code OK if deleting is successful
     */
    @DeleteMapping("{bookId}")
    public ResponseEntity<Void> deleteBook(@PathVariable Long bookId) {
        bookService.deleteBook(bookId);
        return ResponseEntity.ok().build();
    }
}
