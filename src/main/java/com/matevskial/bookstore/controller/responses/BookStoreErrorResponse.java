package com.matevskial.bookstore.controller.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents error response that will be returned by the controller exception handlers
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookStoreErrorResponse {
    private String message;
}
