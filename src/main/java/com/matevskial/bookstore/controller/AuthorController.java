package com.matevskial.bookstore.controller;

import com.matevskial.bookstore.dto.AuthorDto;
import com.matevskial.bookstore.persistence.entity.Author;
import com.matevskial.bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller that exposes the authors resource
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/authors")
public class AuthorController {

    private final AuthorService authorService;

    /**
     * Method to get one author by its id
     *
     * @param authorId - id of the author
     *
     * @return author with specified id wrapped in ResponseEntity if the author exists
     */
    @GetMapping("{authorId}")
    public ResponseEntity<AuthorDto> getAuthorById(@PathVariable long authorId) {
        AuthorDto author = authorService.getAuthorById(authorId);
        return ResponseEntity.ok(author);
    }

    /**
     * Method to persist new author
     *
     * @param author - author to be persisted
     *
     * @return Empty ResponseEntity with status code OK
     */
    @PostMapping
    public ResponseEntity<Void> saveAuthor(@RequestBody AuthorDto author) {
        authorService.saveAuthor(author);
        return ResponseEntity.ok().build();
    }
}
