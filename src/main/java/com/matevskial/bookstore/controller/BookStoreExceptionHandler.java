package com.matevskial.bookstore.controller;

import com.matevskial.bookstore.controller.responses.BookStoreErrorResponse;
import com.matevskial.bookstore.exception.BookStoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Contains method that handle exceptions thrown by methods called from controllers
 */
@ControllerAdvice
public class BookStoreExceptionHandler {

    /**
     *
     * @param e - the exception
     *
     * @return BookStoreErrorResponse wrapped in ResponseEntity
     */
    @ExceptionHandler
    public ResponseEntity<BookStoreErrorResponse> handleException(BookStoreException e) {
        BookStoreErrorResponse errorResponseBody = new BookStoreErrorResponse();
        errorResponseBody.setMessage(e.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseBody);
    }
}
