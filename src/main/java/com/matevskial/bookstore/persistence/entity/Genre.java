package com.matevskial.bookstore.persistence.entity;

public enum Genre {
    FICTION,
    NONFICTION;

    private static final Genre[] copyOfValues = values();

    public static boolean exists(String name) {
        for(Genre g : copyOfValues) {
            if(g.name().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }
}
