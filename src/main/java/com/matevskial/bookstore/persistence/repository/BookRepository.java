package com.matevskial.bookstore.persistence.repository;

import com.matevskial.bookstore.persistence.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {

}
