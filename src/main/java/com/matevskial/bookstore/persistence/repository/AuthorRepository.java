package com.matevskial.bookstore.persistence.repository;

import com.matevskial.bookstore.persistence.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
