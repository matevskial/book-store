INSERT INTO author(first_name, last_name, date_of_birth) VALUES ('Mark', 'Twain', '1835-11-30');
INSERT INTO author(first_name, last_name, date_of_birth) VALUES ('Lewis', 'Carrol', '1832-01-27');
INSERT INTO author(first_name, last_name, date_of_birth) VALUES ('Anthony', 'Burgess', '1917-02-25');

INSERT INTO author(first_name, last_name, date_of_birth) VALUES ('Steven', 'Skiena', '1961-01-30');
INSERT INTO author(first_name, last_name, date_of_birth) VALUES ('George', 'Pólya', '1887-12-13');
INSERT INTO author(first_name, last_name, date_of_birth) values ('Harold', 'Abelson', '1937-04-26');
INSERT INTO author(first_name, last_name, date_of_birth) values ('Gerald', 'Sussman', '1947-02-08');

INSERT INTO book(title, genre) VALUES ('The Adventures of Huckleberry Finn', 'FICTION');
INSERT INTO book(title, genre) VALUES ('Alice''s Adventures in Wonderland', 'FICTION');
INSERT INTO book(title, genre) VALUES ('A Clockwork Orange', 'FICTION');

INSERT INTO book(title, genre) VALUES ('The Algorithm Design Manual', 'NONFICTION');
INSERT INTO book(title, genre) VALUES ('How to Solve It', 'NONFICTION');
INSERT INTO book(title, genre) VALUES ('Structure and interpretation of computer programs', 'NONFICTION');

INSERT INTO book_author(book_id, author_id) values(1, 1);
INSERT INTO book_author(book_id, author_id) values(2, 2);
INSERT INTO book_author(book_id, author_id) values(3, 3);
INSERT INTO book_author(book_id, author_id) values(4, 4);
INSERT INTO book_author(book_id, author_id) values(5, 5);
INSERT INTO book_author(book_id, author_id) values(6, 6);
INSERT INTO book_author(book_id, author_id) values(6, 7);


